-- noinspection SqlNoDataSourceInspectionForFile

CREATE TABLE IF NOT EXISTS movies
(
    id
    INT
    AUTO_INCREMENT
    PRIMARY
    KEY,
    name
    VARCHAR
(
    50
) NOT NULL,
    director
    VARCHAR
(
    50
) NOT NULL,
    minutes INT NOT NULL,
    genre VARCHAR
(
    50
) NOT NULL
    );

insert into movies (name, director, minutes, genre)
values ('Dark Knight', 'Christopher Nolan', 152, 'ACTION'),
       ('Memento', 'Christopher Nolan', 113, 'THRILLER'),
       ('There''s something about Mary', 'Peter Farrelly', 119, 'COMEDY'),
       ('Super 8', 'J. J. Abrams', 112, 'THRILLER'),
       ('Scream', 'Wes Craven', 111, 'HORROR'),
       ('Home Alone', 'Peter Hewitt', 103, 'COMEDY'),
       ('Matrix', 'Lana Wachowski', 136, 'ACTION');
package com.ronny.movie.service;

import com.ronny.movie.data.MovieRepository;
import com.ronny.movie.model.Movie;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static com.ronny.movie.model.Genre.*;
import static org.junit.Assert.assertThat;

public class MovieServiceTest {
    private MovieRepository repository;
    private MovieService movieService;

    @Before
    public void setUp() {
        repository = Mockito.mock(MovieRepository.class);

        Mockito.when(repository.findAll()).thenReturn(Arrays.asList(
                new Movie(1, "Dark Knight", "Christopher Nolan", 152, ACTION),
                new Movie(3, "There's something about Mary", "Peter Farrelly", 119, COMEDY),
                new Movie(2, "Memento", "Christopher Nolan", 113, THRILLER),
                new Movie(4, "Super 8", "J. J. Abrams", 112, THRILLER),
                new Movie(6, "Home Alone", "Peter Hewitt", 103, COMEDY),
                new Movie(5, "Scream", "Wes Craven", 111, HORROR),
                new Movie(7, "Matrix", "Lana Wachowski", 136, ACTION)
        ));
        movieService = new MovieService(repository);
    }

    private List<Integer> getMovieIds(Collection<Movie> movies) {
        return movies.stream().map(Movie::getId).collect(Collectors.toList());
    }


    @Test
    public void findByGenre() {
        Collection<Movie> movies = movieService.findByGenre(COMEDY);
        assertThat(getMovieIds(movies), CoreMatchers.hasItems(6, 3));
    }


    @Test
    public void findByLength() {
        Collection<Movie> movies = movieService.findMovieByLength(120);
        assertThat(getMovieIds(movies), CoreMatchers.hasItems(2, 3, 4, 5, 6));

    }
}
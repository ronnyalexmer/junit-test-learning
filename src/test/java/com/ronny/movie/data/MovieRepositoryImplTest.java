package com.ronny.movie.data;

import com.ronny.movie.model.Movie;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.init.ScriptUtils;

import java.util.Collection;

import static com.ronny.movie.model.Genre.*;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class MovieRepositoryImplTest {

    private DriverManagerDataSource driver;
    private JdbcTemplate jdbcTemplate;
    private MovieRepositoryImpl movieRepository;

    @Before
    public void setUp() throws Exception {
        driver = new DriverManagerDataSource("jdbc:h2:mem:test;MODE=MYSQL", "sa", "sa");
        ScriptUtils.executeSqlScript(driver.getConnection(), new ClassPathResource("sql-scripts/test-data.sql"));
        jdbcTemplate = new JdbcTemplate(driver);
        movieRepository = new MovieRepositoryImpl(jdbcTemplate);
    }

    @After
    public void tearDown() throws Exception {
        driver.getConnection().createStatement().execute("drop all objects delete files");
    }

    @Test
    public void insertMovie() {
        Movie movie = new Movie(8, "Chucky", "Tom Holland", 128, HORROR);
        movieRepository.saveOrUpdate(movie);
        Movie movieInserted = movieRepository.findById(8);
        assertThat(movieInserted, is(movie));
    }

    @Test
    public void loadAllMovies() {
        Collection<Movie> movies = movieRepository.findAll();
        assertThat(movies, hasItems(
                new Movie(1, "Dark Knight", "Christopher Nolan", 152, ACTION),
                new Movie(3, "There's something about Mary", "Peter Farrelly", 119, COMEDY),
                new Movie(2, "Memento", "Christopher Nolan", 113, THRILLER),
                new Movie(4, "Super 8", "J. J. Abrams", 112, THRILLER),
                new Movie(6, "Home Alone", "Peter Hewitt", 103, COMEDY),
                new Movie(5, "Scream", "Wes Craven", 111, HORROR),
                new Movie(7, "Matrix", "Lana Wachowski", 136, ACTION)));
    }

    @Test
    public void loadMovieById() {
        Movie movie = movieRepository.findById(7);
        assertThat(movie, is(new Movie(7, "Matrix", "Lana Wachowski", 136, ACTION)));
    }


    @Test
    public void loadMovieByName() {
        Collection<Movie> movies = movieRepository.findByName("Super");
        assertThat(movies, hasItems(new Movie(4, "Super 8", "J. J. Abrams", 112, THRILLER)));
    }


    @Test
    public void loadMovieByDirector() {
        Collection<Movie> movies = movieRepository.findByDirector("Nolan");
        assertThat(movies, hasItems(new Movie(1, "Dark Knight", "Christopher Nolan", 152, ACTION),
                new Movie(2, "Memento", "Christopher Nolan", 113, THRILLER)));
    }

    @Test
    public void searchMovieByDirectorAndGenre() {
        Movie template = new Movie();
        template.setDirector("Christopher");
        template.setGenre(ACTION);
        Collection<Movie> movies = movieRepository.findByTemplate(template);
        movies.forEach(movie -> System.out.println(movie.toString()));
        assertThat(movies, hasItems(new Movie(1, "Dark Knight", "Christopher Nolan", 152, ACTION)));
    }

    @Test
    public void searchMovieByNameAndMinutes() {
        Movie template = new Movie();
        template.setName("8");
        template.setMinutes(120);
        Collection<Movie> movies = movieRepository.findByTemplate(template);
        movies.forEach(movie -> System.out.println(movie.toString()));
        assertThat(movies, hasItems(new Movie(4, "Super 8", "J. J. Abrams", 112, THRILLER)));
    }


    @Test
    public void searchMovieByNothing() {
        Movie template = new Movie();
        Collection<Movie> movies = movieRepository.findByTemplate(template);
        movies.forEach(movie -> System.out.println(movie.toString()));
        assertThat(movies, hasItems(new Movie(1, "Dark Knight", "Christopher Nolan", 152, ACTION),
                new Movie(3, "There's something about Mary", "Peter Farrelly", 119, COMEDY),
                new Movie(2, "Memento", "Christopher Nolan", 113, THRILLER),
                new Movie(4, "Super 8", "J. J. Abrams", 112, THRILLER),
                new Movie(6, "Home Alone", "Peter Hewitt", 103, COMEDY),
                new Movie(5, "Scream", "Wes Craven", 111, HORROR),
                new Movie(7, "Matrix", "Lana Wachowski", 136, ACTION)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void searchMovieByNegativeMinutes() {
        Movie template = new Movie();
        template.setMinutes(-80);
        movieRepository.findByTemplate(template);
    }

    @Test
    public void searchMovieById() {
        Movie template = new Movie();
        template.setId(5);
        Collection<Movie> movies = movieRepository.findByTemplate(template);
        movies.forEach(movie -> System.out.println(movie.toString()));
        assertThat(movies, hasItems(new Movie(5, "Scream", "Wes Craven", 111, HORROR)));
    }
}
package com.ronny.discounts;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;

import static org.junit.Assert.*;

public class PriceCalculatorTest {

    @Test
    public void totalZeroWhenThereArePrices() {
        PriceCalculator calculator = new PriceCalculator();
        assertThat(calculator.getTotal(), is(0.0));
    }

    @Test
    public void totalIsTheSumOfPrices() {
        PriceCalculator calculator = new PriceCalculator();
        System.out.println(calculator.getDiscount());
        calculator.add(10.2).add(15.5);
        assertThat(calculator.getTotal(), is(25.7));
    }

    @Test
    public void applyDiscountToPrices() {
        PriceCalculator calculator = new PriceCalculator();
        calculator.add(15.5).add(14.5);
        calculator.setDiscount(25);
        assertThat(calculator.getTotal(), is(7.5));
    }


}
package com.ronny.utils;

import org.junit.Assert;
import org.junit.Test;

public class StringUtilsTest {


    @Test
    public void repeatWordZero() {
        Assert.assertEquals("", StringUtils.repeatWord("patata", 0));
    }

    @Test
    public void repeatWordOnce() {
        Assert.assertEquals("patata", StringUtils.repeatWord("patata", 1));
    }

    @Test
    public void repeatWordMultiTimes() {
        Assert.assertEquals("patatapatata", StringUtils.repeatWord("patata", 2));
    }

    @Test(expected = IllegalArgumentException.class)
    public void repeatWordNegative() {
        StringUtils.repeatWord("patata", -2);
    }

    @Test
    public void isNotEmptyWhenIsAWord() {
        Assert.assertFalse(StringUtils.isEmpty("patata"));
    }

    @Test
    public void isEmptyWhenIsEmpty() {
        Assert.assertTrue(StringUtils.isEmpty(""));
    }

    @Test
    public void isEmptyWhenOnlySpaces() {
        Assert.assertTrue(StringUtils.isEmpty("     "));
    }

    @Test
    public void isEmptyWhenIsNull() {
        Assert.assertTrue(StringUtils.isEmpty(null));
    }
}
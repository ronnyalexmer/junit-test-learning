package com.ronny.utils;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;

import static org.junit.Assert.*;

public class DateUtilsTest {

    /**
     * All years divisible by 400 are leap years (1600, 2000, 2400)
     */
    @Test
    public void isLeapYearWhenIsDivisibleBy400() {
        assertThat(DateUtils.isLeapYear(1600), is(true));
        assertThat(DateUtils.isLeapYear(2000), is(true));
        assertThat(DateUtils.isLeapYear(2400), is(true));
    }

    /**
     * All years divisible by 100 but NOT by 400 are NOT leaps (1700, 1800, 1900)
     */
    @Test
    public void isNotLeapYearWhenIsDivisibleBy100NotBy400() {
        assertThat(DateUtils.isLeapYear(1700), is(false));
        assertThat(DateUtils.isLeapYear(1800), is(false));
        assertThat(DateUtils.isLeapYear(1900), is(false));
    }

    /**
     * All years divisible by 4 are leaps but NOT by 100 (1996, 2004, 2012)
     */
    @Test
    public void isLeapYearWhenIsDivisibleBy4ByNotBy100() {
        assertThat(DateUtils.isLeapYear(1996), is(true));
        assertThat(DateUtils.isLeapYear(2004), is(true));
        assertThat(DateUtils.isLeapYear(2012), is(true));
    }

    /**
     * All the years that are NOT divisible by 4 are NOT leaps (2017, 2018, 2019)
     */
    @Test
    public void isNotLeapYearWhenIsNotDivisibleBy4() {
        assertThat(DateUtils.isLeapYear(2017), is(false));
        assertThat(DateUtils.isLeapYear(2018), is(false));
        assertThat(DateUtils.isLeapYear(2019), is(false));
    }


}
package com.ronny.movie.service;

import com.ronny.movie.data.MovieRepository;
import com.ronny.movie.model.Genre;
import com.ronny.movie.model.Movie;

import java.util.Collection;
import java.util.stream.Collectors;

public class MovieService {

    private MovieRepository movieRepository;

    public MovieService(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

    public Collection<Movie> findByGenre(Genre genre) {
        return movieRepository.findAll().stream().
                filter(movie -> movie.getGenre() == genre).collect(Collectors.toList());
    }

    public Collection<Movie> findMovieByLength(int length) {
        return movieRepository.findAll().stream().
                filter(movie -> movie.getMinutes() <= length).collect(Collectors.toList());
    }
}

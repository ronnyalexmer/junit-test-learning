package com.ronny.movie.data;

import com.ronny.movie.model.Genre;
import com.ronny.movie.model.Movie;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import static com.ronny.movie.MovieConstants.*;

public class MovieRepositoryImpl implements MovieRepository {

    private JdbcTemplate jdbcTemplate;

    public MovieRepositoryImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Movie findById(long id) {
        Object[] args = {id};
        return (Movie) jdbcTemplate.queryForObject("select * from movies where id = ?", args, movieMapper);
    }

    @Override
    public Collection<Movie> findByName(String name) {
        Object[] args = {likeField(name)};
        ArrayList<Movie> movies = new ArrayList<>();
        List<Map<String, Object>> results = jdbcTemplate.queryForList("select * from movies where LOWER(name) LIKE LOWER(?)", args, new int[]{Types.VARCHAR});
        results.forEach(movieMapper -> movies.add(
                new Movie(
                        (int) movieMapper.get(ID),
                        (String) movieMapper.get(NAME),
                        (String) movieMapper.get(DIRECTOR),
                        (int) movieMapper.get(MINUTES),
                        Genre.valueOf((String) movieMapper.get(GENRE)))
        ));
        return movies;
    }

    @Override
    public Collection<Movie> findByDirector(String directorName) {
        Object[] args = {likeField(directorName)};
        ArrayList<Movie> movies = new ArrayList<>();
        List<Map<String, Object>> results = jdbcTemplate.queryForList("select * from movies where LOWER(director) LIKE LOWER(?)", args, new int[]{Types.VARCHAR});
        results.forEach(movieMapper -> movies.add(
                new Movie(
                        (int) movieMapper.get(ID),
                        (String) movieMapper.get(NAME),
                        (String) movieMapper.get(DIRECTOR),
                        (int) movieMapper.get(MINUTES),
                        Genre.valueOf((String) movieMapper.get(GENRE)))
        ));
        return movies;
    }

    public String likeField(String field) {
        return "%" + field + "%";
    }

    @Override
    public Collection<Movie> findByTemplate(Movie template) {
        ArrayList<Movie> movies = new ArrayList<>();
        if (template.getId() != null) {
            movies.add(findById(template.getId()));
        } else {
            Integer minutes = template.getMinutes();
            String name = template.getName();
            String director = template.getDirector();
            Genre genre = template.getGenre();

            StringBuffer sql = new StringBuffer("select * from movies");
            ArrayList<Object> args = new ArrayList<>();
            if (name != null || director != null || minutes != null || genre != null) {
                sql.append(" where ");
                if (name != null) {
                    args.add(likeField(name));
                    sql.append("LOWER(name) LIKE LOWER(?)");
                }
                if (director != null) {
                    if (args.size() > 0) {
                        sql.append(" AND ");
                    }
                    args.add(likeField(director));
                    sql.append("LOWER(director) LIKE LOWER(?)");
                }
                if (minutes != null) {
                    if (args.size() > 0) {
                        sql.append(" AND ");
                    }else {
                        throw new IllegalArgumentException("Minutes must be positive");
                    }
                    args.add(minutes);
                    sql.append("minutes <= ?");
                }
                if (genre != null) {
                    if (args.size() > 0) {
                        sql.append(" AND ");
                    }
                    args.add(genre.toString());
                    sql.append("LOWER(genre) = LOWER(?)");
                }
            }
            System.out.println("SQL: " + sql.toString());
            args.forEach(arg -> System.out.println("arg: " + arg));
            List<Map<String, Object>> results = jdbcTemplate.queryForList(sql.toString(), args.toArray());
            System.out.println("Resultados: " + results.size());
            results.forEach(movieMapper -> movies.add(
                    new Movie(
                            (int) movieMapper.get(ID),
                            (String) movieMapper.get(NAME),
                            (String) movieMapper.get(DIRECTOR),
                            (int) movieMapper.get(MINUTES),
                            Genre.valueOf((String) movieMapper.get(GENRE)))
            ));
        }

        return movies;
    }

    @Override
    public Collection<Movie> findAll() {
        return jdbcTemplate.query("select * from movies", movieMapper);
    }

    @Override
    public void saveOrUpdate(Movie movie) {
        jdbcTemplate.update("insert into movies(name, director, minutes, genre) values(?,?,?,?)",
                movie.getName(), movie.getDirector(), movie.getMinutes(), movie.getGenre().toString());
    }

    private static RowMapper movieMapper = (resultSet, i) -> new Movie(
            resultSet.getInt(ID),
            resultSet.getString(NAME),
            resultSet.getString(DIRECTOR),
            resultSet.getInt(MINUTES),
            Genre.valueOf(resultSet.getString(GENRE))
    );
}

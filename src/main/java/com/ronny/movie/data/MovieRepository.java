package com.ronny.movie.data;

import com.ronny.movie.model.Movie;

import java.util.Collection;

public interface MovieRepository {
    Movie findById(long id);

    Collection<Movie> findByName(String name);

    Collection<Movie> findByDirector(String name);
    Collection<Movie> findByTemplate(Movie template);

    Collection<Movie> findAll();

    void saveOrUpdate(Movie movie);
}

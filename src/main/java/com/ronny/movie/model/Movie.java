package com.ronny.movie.model;

import java.util.Objects;

public class Movie {
    private Integer id;
    private String name;
    private String director;
    private Integer minutes;
    private Genre genre;

    public Movie(String name, String director, int minutes, Genre genre) {
        this(null, name, director, minutes, genre);
    }


    public Movie(Integer id, String name, String director, int minutes, Genre genre) {
        this.id = id;
        this.name = name;
        this.director = director;
        this.minutes = minutes;
        this.genre = genre;
    }

    public Movie() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getMinutes() {
        return minutes;
    }

    public void setMinutes(Integer minutes) {
        this.minutes = minutes;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", director='" + director + '\'' +
                ", minutes=" + minutes +
                ", genre=" + genre +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Movie movie = (Movie) o;
        return Objects.equals(id, movie.id) && Objects.equals(name, movie.name) && Objects.equals(director, movie.director) && Objects.equals(minutes, movie.minutes) && genre == movie.genre;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, director, minutes, genre);
    }
}

package com.ronny.movie;

public class MovieConstants {
    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String DIRECTOR = "director";
    public static final String MINUTES = "minutes";
    public static final String GENRE = "genre";
}

package com.ronny.utils;

public class StringUtils {

    public static String repeatWord(String word, int times) {
        if (times < 0) {
            throw new IllegalArgumentException("times arg must positive");
        }
        StringBuffer str = new StringBuffer();
        for (int i = 0; i < times; i++) {
            str.append(word);
        }
        return str.toString();
    }

    public static boolean isEmpty(String str) {
        return str == null || str.trim().length() == 0;
    }
}

package com.ronny.payments;

public class PaymentProcessor {
    private PaymentGateway paymentGateway;

    public PaymentProcessor(PaymentGateway paymentGateway) {
        this.paymentGateway = paymentGateway;
    }

    public boolean makePayment(double amount) {
        PaymentRequest paymentRequest = new PaymentRequest(amount);
        PaymentResponse paymentResponse = paymentGateway.requestPayment(paymentRequest);
        return paymentResponse.getStatus() == PaymentResponse.PaymentStatus.OK;
    }
}

package com.ronny.discounts;

import java.util.ArrayList;
import java.util.List;

public class PriceCalculator {
    private List<Double> prices = new ArrayList<>();
    private double discount;

    public double getTotal() {
        double total = prices.stream().mapToDouble(price -> price).sum();
        if (discount != 0.0) {
            total *= (discount / 100);
        }
        return total;
    }

    public List<Double> add(double price) {
        prices.add(price);
        return prices;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getDiscount() {
        return discount;
    }
}
